import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private service: CommonService) { }

  ngOnInit(): void {
  }
  logout()
  {
    localStorage.removeItem('token'); 
  } 
}
