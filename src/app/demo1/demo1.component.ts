import { Component, OnInit,NgModule } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { map, withLatestFrom, startWith, tap } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.css']
})
export class Demo1Component implements OnInit {
  foods$: Observable<FoodItem[]>;
  filteredFoods$: Observable<FoodItem[]>;

  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder){
    this.formGroup = formBuilder.group({filter: [""]});
    
    this.foods$ = this.getFoods();

    this.filteredFoods$ = this.formGroup.controls["filter"]
      .valueChanges
      .pipe(
        startWith(''),
        withLatestFrom(this.foods$), 
        map(([val, foods]) => !val ? foods : foods.filter(x => x.name.toLowerCase().includes(val))),
      )

      this.filteredFoods$ = this.formGroup.controls["filter"]
      .valueChanges
      .pipe(
        startWith(''),
        withLatestFrom(this.foods$), 
        map(([val, foods]) => !val ? foods : foods.filter(x => x.email.toLowerCase().includes(val))),
      )
  }
  ngOnInit(): void {
    
  }

  private getFoods(){
    return of( 
      [
        { name: "abhishek", email:'abhishek@gmail.com', phone:7282939103, gender:'male' },
        { name: "test1", email:'test1@gmail.com', phone:72732985723, gender:'female' },
        { name: "test2", email:'test2@gmail.com', phone:35235339103, gender:'female' },
        { name: "test3", email:'test3@gmail.com', phone:721139103, gender:'male' },
        { name: "test4", email:'test4@gmail.com', phone:929239103, gender:'male' },
        { name: "test5", email:'test5@gmail.com', phone:89121939103, gender:'female' },
        { name: "test6", email:'test6@gmail.com', phone:2221093672, gender:'female'}
      ]
    ).pipe(tap(console.log))
  }
  logout()
  {
    localStorage.removeItem('token'); 
  }  
}

export interface FoodItem {
  name:string;
  email:string;
  phone:number;
  gender:string;
}