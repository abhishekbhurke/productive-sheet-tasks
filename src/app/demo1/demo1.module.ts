import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Demo1Component } from './demo1.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';

const routes: Routes = [
  {
    path:'',
    component: Demo1Component
    // canActivate:[ReverseGuard]
  }
]

@NgModule({
  declarations: [
    Demo1Component,
    SearchPipe
  ],
  exports: [
    // ...
    SearchPipe,
  ],
  imports: [RouterModule.forChild(routes),
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class Demo1Module { 
  
}
